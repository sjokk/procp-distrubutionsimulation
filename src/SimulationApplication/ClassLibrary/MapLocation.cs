﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ClassLibrary
{
    class MapLocation
    {
        public Map Map { get; set; }
        public Location Location { get; set; }
        public Building Building { get; set; }
    }
}
